package io.novajox.bitrise.artifact

import android.databinding.Bindable
import io.novajox.BR
import io.novajox.data.pojo.BitriseArtifact
import io.novajox.mvpvm.vm.BaseVM

/**
 * Created by novajox on 09/01/18.
 */
class ArtifactVM(model: BitriseArtifact) : BaseVM<BitriseArtifact>(model) {

    val title: String
        @Bindable
        get() = model.title

    val url: String
        @Bindable
        get() = model.expiringDownloadUrl


    val slug: String
        @Bindable
        get() = model.slug

    fun update(it: BitriseArtifact?) {

        model.expiringDownloadUrl = it!!.expiringDownloadUrl
        model.publicInstallPageUrl = it.publicInstallPageUrl
        notifyPropertyChanged(BR.url)
    }
}