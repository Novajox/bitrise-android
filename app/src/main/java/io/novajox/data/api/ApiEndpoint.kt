package io.novajox.data.api

import io.novajox.data.pojo.*
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * Created by novajox on 18/12/17.
 */
interface ApiEndpoint {

    @GET("me")
    fun me(): Observable<BitriseResponse<BitriseUser>>


    @GET("me/apps")
    fun myApps(): Observable<BitriseResponse<List<BitriseProject>>>


    @GET("apps/{appSlug}/builds")
    fun appBuilds(@Path("appSlug") appSlug: String,
                  @Query("limit") limit: Int)
            : Observable<BitriseResponse<List<BitriseBuild>>>

    @GET("apps/{appSlug}/builds/{buildSlug}/artifacts")
    fun buildArtifacts(@Path("appSlug") appSlug: String, @Path("buildSlug") buildSlug: String,
                       @Query("limit") limit: Int)
            : Observable<BitriseResponse<List<BitriseArtifact>>>

    @GET("apps/{appSlug}/builds/{buildSlug}/artifacts/{artifactSlug}")
    fun artifactDetail(@Path("appSlug") appSlug: String, @Path("buildSlug") buildSlug: String,
                       @Path("artifactSlug") artifactSlug: String,
                       @Query("limit") limit: Int)
            : Observable<BitriseResponse<BitriseArtifact>>

    @POST("app/{appSlug}/build/start.json")
    fun startJob(@Path("appSlug") appSlug: String)
}