package io.novajox.utils

import android.util.Log
import io.novajox.BuildConfig

/**
 * Created by novajox on 05/01/18.
 */
object AppLog {

    fun d(message: String) {
        if (BuildConfig.DEBUG) {
            Log.d("Bitrise-App", message)
        }
    }

    // other logging methods
}