package io.novajox.mvpvm.androidcomponent

import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.novajox.mvpvm.presenter.IPresenter
import io.novajox.mvpvm.view.IView

/**
 * Created by novajox on 29/12/17.
 */

abstract class BaseFragmentView<P : IPresenter, B : ViewDataBinding> : Fragment(), IView {

    internal var presenter: P? = null
    internal var binding: B? = null

    val showLoadingState: ObservableBoolean = ObservableBoolean(false)
    val showErrorState: ObservableBoolean = ObservableBoolean(false)
    val showEmptyState: ObservableBoolean = ObservableBoolean(false)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = DataBindingUtil.inflate(inflater, layout(), container, false)
        binding!!.setVariable(binding(), this)
        presenter = createPresenter()
        return binding!!.root
    }

    internal abstract fun createPresenter(): P

    override fun showLoadingState() {
        showLoadingState.set(true)
    }

    override fun hideLoadingState() {
        showLoadingState.set(false)
    }

    override fun showEmptyState() {
        showEmptyState.set(true)
    }

    override fun hideEmptyState() {
        showEmptyState.set(false)
    }

    override fun showErrorState() {
        showErrorState.set(true)
    }

    override fun hideErrorState() {
        showErrorState.set(false)
    }

    abstract fun layout(): Int
    abstract fun binding(): Int
}
