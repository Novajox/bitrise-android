package io.novajox.mvpvm.androidcomponent

import android.databinding.DataBindingUtil
import android.databinding.ObservableBoolean
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

import io.novajox.mvpvm.presenter.IPresenter
import io.novajox.mvpvm.view.IView

/**
 * Created by novajox on 29/12/17.
 */

abstract class BaseActivityView<P : IPresenter, B : ViewDataBinding> : AppCompatActivity(), IView {

    internal var presenter: P? = null
    internal var binding: B? = null

    val showLoadingState: ObservableBoolean = ObservableBoolean(false)
    val showErrorState: ObservableBoolean = ObservableBoolean(false)
    val showEmptyState: ObservableBoolean = ObservableBoolean(false)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layout())
        binding!!.setVariable(binding(), this)
        createPresenter()
    }

    internal abstract fun createPresenter()

    override fun showLoadingState() {
        showLoadingState.set(true)
    }

    override fun hideLoadingState() {
        showLoadingState.set(false)
    }

    override fun showEmptyState() {
        showEmptyState.set(true)
    }

    override fun hideEmptyState() {
        showEmptyState.set(false)
    }

    override fun showErrorState() {
        showErrorState.set(true)
    }

    override fun hideErrorState() {
        showErrorState.set(false)
    }

    abstract fun layout(): Int
    abstract fun binding(): Int
}
