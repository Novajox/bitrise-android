package io.novajox.mvpvm.view

import android.databinding.BaseObservable
import android.databinding.ObservableBoolean
import io.novajox.mvpvm.presenter.IPresenter

/**
 * Created by novajox on 05/01/18.
 */
abstract class BaseView<P : IPresenter> : IView, BaseObservable() {

    lateinit var presenter: P
    val showLoadingState: ObservableBoolean = ObservableBoolean(false)
    val showErrorState: ObservableBoolean = ObservableBoolean(false)
    val showEmptyState: ObservableBoolean = ObservableBoolean(false)

    override fun showLoadingState() {
        showLoadingState.set(true)
    }

    override fun hideLoadingState() {
        showLoadingState.set(false)
    }

    override fun showEmptyState() {
        showEmptyState.set(true)
    }

    override fun hideEmptyState() {
        showEmptyState.set(false)
    }

    override fun showErrorState() {
        showErrorState.set(true)
    }

    override fun hideErrorState() {
        showErrorState.set(false)
    }


}