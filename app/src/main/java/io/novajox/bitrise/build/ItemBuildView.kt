package io.novajox.bitrise.build

import android.view.View
import android.widget.Toast
import io.novajox.mvpvm.view.BaseView


/**
 * Created by novajox on 06/01/18.
 */
class ItemBuildView(val vm: BuildVM) : BaseView<ItemBuildPresenter>() {

    fun onClick(view: View) {
        if (!vm.running)
            presenter.onClickBuild()
        else
            Toast.makeText(view.context, vm.statusText, Toast.LENGTH_SHORT).show()
    }


}