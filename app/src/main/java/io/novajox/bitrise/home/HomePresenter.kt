package io.novajox.bitrise.home

import android.util.Log
import io.novajox.data.api.ApiManager
import io.novajox.mvpvm.presenter.BasePresenter

/**
 * Created by novajox on 29/12/17.
 */
class HomePresenter(view: HomeContract.HomeView) : BasePresenter<HomeContract.HomeView>(view) {


    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.myApps().subscribe({
            view.addItems(it)
            view.hideLoadingState()
        }, {
            view.hideLoadingState()
            view.showErrorState()
            Log.e("HOME", "$ { it.localizedMessage }")
        })
    }

}