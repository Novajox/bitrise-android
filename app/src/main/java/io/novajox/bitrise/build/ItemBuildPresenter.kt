package io.novajox.bitrise.build

import io.novajox.mvpvm.presenter.IPresenter

/**
 * Created by novajox on 06/01/18.
 */
abstract class ItemBuildPresenter : IPresenter {

    override fun loadData() {

    }

    abstract fun onClickDownload()

    abstract fun onClickBuild()
}