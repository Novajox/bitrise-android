package io.novajox.bitrise.artifact

import io.novajox.bitrise.artifact.item.ItemArtifactView
import io.novajox.data.api.ApiManager
import io.novajox.data.pojo.BitriseBuild
import io.novajox.data.pojo.BitriseProject
import io.novajox.mvpvm.presenter.BasePresenter

/**
 * Created by novajox on 07/01/18.
 */
class ArtifactsPresenter(view: ArtifactsView) : BasePresenter<ArtifactsView>(view) {


    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.buildArtifacts(view.projectVM.model.slug, view.buildVM.model.slug, 50).subscribe({
            val list = arrayListOf<ItemArtifactView>()
            for (item in it)
                list.add(ItemArtifactView(view.activity, view.projectVM, view.buildVM,ArtifactVM(item)))

            view.addItems(list)
            view.hideLoadingState()
        }, {
            view.hideLoadingState()
            view.showErrorState()
        })
    }
}