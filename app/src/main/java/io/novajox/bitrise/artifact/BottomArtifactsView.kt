package io.novajox.bitrise.artifact

import android.app.Dialog
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.BottomSheetBehavior
import android.support.design.widget.BottomSheetDialog
import android.support.design.widget.BottomSheetDialogFragment
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import io.novajox.R
import io.novajox.bitrise.build.BuildVM
import io.novajox.bitrise.project.ProjectVM
import io.novajox.data.pojo.BitriseBuild
import io.novajox.data.pojo.BitriseProject
import io.novajox.databinding.BottomArtifactsViewBinding
import io.novajox.utils.EXTRA
import me.tatarka.bindingcollectionadapter2.BR

/**
 * Created by novajox on 07/01/18.
 */
class BottomArtifactsView : BottomSheetDialogFragment() {

    private lateinit var artifactsView: ArtifactsView

    companion object {
        fun newInstance(project: BitriseProject, build: BitriseBuild): BottomArtifactsView {
            val args = Bundle()
            args.putParcelable(EXTRA.BUILD, build)
            args.putParcelable(EXTRA.PROJECT, project)
            val f = BottomArtifactsView()
            f.arguments = args
            return f
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = DataBindingUtil.inflate<BottomArtifactsViewBinding>(inflater, R.layout.bottom_artifacts_view, container, false)
        artifactsView = ArtifactsView(activity!!, BuildVM(arguments!!.getParcelable(EXTRA.BUILD)),(ProjectVM( arguments!!.getParcelable(EXTRA.PROJECT))))
        artifactsView.presenter = ArtifactsPresenter(artifactsView)
        view.setVariable(BR.artifactsView, artifactsView)
        return view.root
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState) as BottomSheetDialog

        dialog.setOnShowListener { dialog1 ->
            val d = dialog1 as BottomSheetDialog
            val bottomSheet = d.findViewById<View>(android.support.design.R.id.design_bottom_sheet) as FrameLayout?
            bottomSheet!!.setBackgroundColor(ContextCompat.getColor(this@BottomArtifactsView.activity!!, android.R.color.transparent))
            BottomSheetBehavior.from(bottomSheet).setState(BottomSheetBehavior.STATE_EXPANDED)
        }
        return dialog
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        artifactsView.presenter.loadData()
    }


}