package io.novajox.data

import com.orhanobut.hawk.Hawk

import io.novajox.bitrise.App
import io.novajox.data.model.AppUser

/**
 * Created by novajox on 13/12/17.
 */

object AppPrefs {

    private val TOKEN = "TOKEN"
    private val USER = "USER"

    fun init(context: App) {
        Hawk.init(context).build()
    }

    fun hasToken(): Boolean {
        return Hawk.contains(TOKEN)
    }


    fun putToken(token: String): Boolean {
        return Hawk.put(TOKEN, token)
    }

    fun token(): String {
        return Hawk.get(TOKEN)
    }

    fun putAppUser(appUser: AppUser) {
        Hawk.put(USER, appUser)
    }

    fun user() : AppUser {
        return Hawk.get(USER)
    }
}
