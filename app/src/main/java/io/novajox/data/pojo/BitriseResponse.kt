package io.novajox.data.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by novajox on 29/12/17.
 */

data class BitriseResponse<T>(
        @SerializedName("data") var data: T,
        @SerializedName("paging") var paging:BitrisePaging
)
