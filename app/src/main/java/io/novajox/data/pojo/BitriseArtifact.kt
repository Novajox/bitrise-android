package io.novajox.data.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by novajox on 09/01/18.
 */
data class BitriseArtifact(
        @SerializedName("title") val title: String,
        @SerializedName("artifact_type") val artifactType: String,
        @SerializedName("expiring_download_url") var expiringDownloadUrl: String,
        @SerializedName("is_public_page_enabled") val isPublicPageEnabled: Boolean,
        @SerializedName("slug") val slug: String,
        @SerializedName("public_install_page_url") var publicInstallPageUrl: String,
        @SerializedName("file_size_bytes") val fileSizeBytes: Int
)