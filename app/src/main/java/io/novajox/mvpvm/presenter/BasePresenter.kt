package io.novajox.mvpvm.presenter

import io.novajox.mvpvm.view.IView
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by novajox on 29/12/17.
 */
abstract class BasePresenter<V : IView>
(protected var view: V) : IPresenter {
    val disposable = CompositeDisposable()
}
