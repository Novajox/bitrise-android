package io.novajox.data.pojo
import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName


/**
 * Created by novajox on 30/12/17.
 */
data class BitriseProject(
		@SerializedName("slug") val slug: String,
		@SerializedName("title") val title: String,
		@SerializedName("project_type") val projectType: String,
		@SerializedName("provider") val provider: String,
		@SerializedName("repo_owner") val repoOwner: String,
		@SerializedName("repo_url") val repoUrl: String,
		@SerializedName("repo_slug") val repoSlug: String,
		@SerializedName("is_disabled") val isDisabled: Boolean
): Parcelable {
	constructor(parcel: Parcel) : this(
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readString(),
			parcel.readByte() != 0.toByte()) {
	}

	override fun writeToParcel(parcel: Parcel, flags: Int) {
		parcel.writeString(slug)
		parcel.writeString(title)
		parcel.writeString(projectType)
		parcel.writeString(provider)
		parcel.writeString(repoOwner)
		parcel.writeString(repoUrl)
		parcel.writeString(repoSlug)
		parcel.writeByte(if (isDisabled) 1 else 0)
	}

	override fun describeContents(): Int {
		return 0
	}

	companion object CREATOR : Parcelable.Creator<BitriseProject> {
		override fun createFromParcel(parcel: Parcel): BitriseProject {
			return BitriseProject(parcel)
		}

		override fun newArray(size: Int): Array<BitriseProject?> {
			return arrayOfNulls(size)
		}
	}
}