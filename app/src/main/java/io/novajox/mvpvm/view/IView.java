package io.novajox.mvpvm.view;

/**
 * Created by novajox on 13/12/17.
 */

public interface IView {

    void showLoadingState();

    void hideLoadingState();

    void showEmptyState();

    void hideEmptyState();

    void showErrorState();

    void hideErrorState();
}
