package io.novajox.data.pojo

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.util.*


/**
 * Created by blackbird-linux on 03/01/18.
 */
data class BitriseBuild(
        @SerializedName("triggered_at") val triggeredAt: Date, //2017-12-09T20:00:24Z
        @SerializedName("started_on_worker_at") val startedOnWorkerAt: Date, //2017-12-09T20:00:25Z
        @SerializedName("environment_prepare_finished_at") val environmentPrepareFinishedAt: Date, //2017-12-09T20:00:25Z
        @SerializedName("finished_at") val finishedAt: Date, //2017-12-09T20:04:27Z
        @SerializedName("slug") val slug: String, //8c806a172060284b
        @SerializedName("status") val status: Int, //1
        @SerializedName("status_text") val statusText: String, //success
        @SerializedName("abort_reason") val abortReason: String, //null
        @SerializedName("is_on_hold") val isOnHold: Boolean, //false
        @SerializedName("branch") val branch: String, //master
        @SerializedName("build_number") val buildNumber: Int, //1
        @SerializedName("commit_hash") val commitHash: String, //null
        @SerializedName("commit_message") val commitMessage: String, //null
        @SerializedName("tag") val tag: String, //null
        @SerializedName("triggered_workflow") val triggeredWorkflow: String, //primary
        @SerializedName("triggered_by") val triggeredBy: String, //manual-Novajox
        @SerializedName("stack_config_type") val stackConfigType: String, //standard1
        @SerializedName("stack_identifier") val stackIdentifier: String, //linux-docker-android
        @SerializedName("pull_request_id") val pullRequestId: Int, //0
        @SerializedName("pull_request_target_branch") val pullRequestTargetBranch: String, //null
        @SerializedName("pull_request_view_url") val pullRequestViewUrl: String, //null
        @SerializedName("commit_view_url") val commitViewUrl: String//null
) : Parcelable {
    constructor(parcel: Parcel) : this(
            parcel.readSerializable() as Date,
            parcel.readSerializable() as Date,
            parcel.readSerializable() as Date,
            parcel.readSerializable() as Date,
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readByte() != 0.toByte(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readInt(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString()) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeSerializable(triggeredAt)
        parcel.writeSerializable(startedOnWorkerAt)
        parcel.writeSerializable(environmentPrepareFinishedAt)
        parcel.writeSerializable(finishedAt)
        parcel.writeString(slug)
        parcel.writeInt(status)
        parcel.writeString(statusText)
        parcel.writeString(abortReason)
        parcel.writeByte(if (isOnHold) 1 else 0)
        parcel.writeString(branch)
        parcel.writeInt(buildNumber)
        parcel.writeString(commitHash)
        parcel.writeString(commitMessage)
        parcel.writeString(tag)
        parcel.writeString(triggeredWorkflow)
        parcel.writeString(triggeredBy)
        parcel.writeString(stackConfigType)
        parcel.writeString(stackIdentifier)
        parcel.writeInt(pullRequestId)
        parcel.writeString(pullRequestTargetBranch)
        parcel.writeString(pullRequestViewUrl)
        parcel.writeString(commitViewUrl)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BitriseBuild> {
        override fun createFromParcel(parcel: Parcel): BitriseBuild {
            return BitriseBuild(parcel)
        }

        override fun newArray(size: Int): Array<BitriseBuild?> {
            return arrayOfNulls(size)
        }
    }
}
