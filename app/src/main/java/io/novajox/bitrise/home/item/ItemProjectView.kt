package io.novajox.bitrise.home.item

import android.databinding.Bindable
import android.view.View
import io.novajox.R
import io.novajox.bitrise.build.BuildVM
import io.novajox.bitrise.project.ProjectVM
import io.novajox.data.pojo.BitriseBuild
import io.novajox.mvpvm.view.BaseView

/**
 * Created by novajox on 05/01/18.
 */

class ItemProjectView(var vm: ProjectVM) : BaseView<ItemProjectPresenter>() {
    var buildVM: BuildVM? = null

    fun update(build: BitriseBuild?) {
        buildVM = BuildVM(build!!)
        notifyChange()
    }


    fun onClick(view: View) {
        presenter.onClickProject()
    }

    @Bindable
    fun getColorStatus(): Int {
        if (buildVM == null) return R.color.colorLoading
        return buildVM!!.colorStatus
    }
}
