package io.novajox.utils

/**
 * Created by novajox on 13/12/17.
 */

object EXTRA {
    val URL = "URL"
    val PROJECT = "PROJECT"
    val BUILD = "BUILD"
}
