package io.novajox.mvpvm.vm

import android.databinding.BaseObservable

/**
 * Created by novajox on 04/01/18.
 */
open class BaseVM<T>(public val model: T) : BaseObservable() {


}