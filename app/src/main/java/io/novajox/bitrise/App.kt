package io.novajox.bitrise

import android.app.Application
import com.liulishuo.filedownloader.FileDownloader

import io.novajox.data.AppPrefs
import io.novajox.data.api.ApiManager
import net.danlew.android.joda.JodaTimeAndroid

/**
 * Created by novajox on 13/12/17.
 */

class App : Application() {
    override fun onCreate() {
        super.onCreate()
        ApiManager.INSTANCE.init()
        AppPrefs.init(this)
        FileDownloader.setup(this)
        JodaTimeAndroid.init(this);
    }
}
