package io.novajox.mvpvm.databinding

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import org.joda.time.Interval
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by novajox on 04/01/18.
 */

object Bindings {

    const val DF_SIMPLE_STRING = "yyyy-MM-dd HH:mm:ss"

    @JvmStatic
    @BindingAdapter("isVisible")
    fun setIsVisible(view: View, isVisible: Boolean) {
        view.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter("backgroundColor")
    fun setBackgroundColor(view: View, color: Int) {
        view.setBackgroundColor(ContextCompat.getColor(view.context, color))
    }


    @JvmStatic
    @BindingAdapter("textColor")
    fun setBackgroundColor(view: TextView, color: Int) {
        view.setTextColor(ContextCompat.getColor(view.context, color))
    }


    @JvmStatic
    @BindingAdapter("formatDate")
    fun formatDate(view: TextView, date: Date?) {
        if (date == null)
            return

        val startDate = Date()
        val interval = Interval(date.time, startDate.time)
        val period = interval.toDuration()
        val dateParser = SimpleDateFormat("HH:mm")

        var dateText = ""
        if (period.standardDays >= 30) dateText = "More than 30 days ago"
        if (period.standardDays in 1..1) dateText = "Yesterday, at ${dateParser.format(date)}"
        if (period.standardDays in 2..29) dateText = "${period.standardDays} day(s) ago, at ${dateParser.format(date)}"
        if (period.standardHours < 24) dateText = "${period.standardHours} hour(s) ago"
        if (period.standardMinutes < 60) dateText = "${period.standardMinutes} minutes ago"
        if (period.standardSeconds < 60) dateText = "${period.standardSeconds} seconds ago"
        view.text = dateText
    }

    @JvmStatic
    @BindingAdapter("imageUrl")
    fun downloadImage(view: ImageView, url: String?) {
        if (url == null)
            return
        Glide.with(view).load(url).into(view)
    }
}
