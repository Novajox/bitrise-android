package io.novajox.bitrise.artifact.item

import android.app.Activity
import android.databinding.ObservableBoolean
import android.view.View
import android.widget.Toast
import io.novajox.R
import io.novajox.bitrise.artifact.ArtifactVM
import io.novajox.bitrise.build.BuildVM
import io.novajox.bitrise.download.DownloadVM
import io.novajox.bitrise.project.ProjectVM
import io.novajox.mvpvm.view.BaseView

/**
 * Created by novajox on 09/01/18.
 */
class ItemArtifactView(val activity: Activity, val projectVM: ProjectVM, val buildVM: BuildVM, val artifactVM: ArtifactVM) : BaseView<ItemArtifactPresenter>() {

    val downloading = ObservableBoolean()
    val downloaded = ObservableBoolean()
    val errorDownloading = ObservableBoolean()

    var downloadVM = DownloadVM()

    fun onClick(view: View) {
        if (!downloading.get() && !downloaded.get())
            presenter.onClickDownload(activity)
        if (!downloading.get() && downloaded.get())
            presenter.onClickInstall(activity)
        if (downloading.get())
            showMessageAlreadyDownloading()
    }


    fun showDownloadingState() {
        downloading.set(true)
    }

    fun hideDownloadingState() {
        downloading.set(false)
    }

    fun showDownloadedState() {
        downloaded.set(true)
    }

    fun hideDownloadedState() {
        downloaded.set(false)
    }

    fun showErrorDownloadingState() {
        errorDownloading.set(true)
    }

    fun updateDownload(soFarBytes: Int, totalBytes: Int) {
        downloadVM.update(soFarBytes, totalBytes)
    }

    fun showMessageAlreadyDownloading() {
        Toast.makeText(activity, R.string.download, Toast.LENGTH_SHORT).show()
    }


}