package io.novajox.bitrise.project.detail

import android.databinding.ObservableArrayList
import android.os.Bundle
import android.widget.Toast
import io.novajox.BR
import io.novajox.R
import io.novajox.bitrise.artifact.BottomArtifactsView
import io.novajox.bitrise.build.BuildVM
import io.novajox.bitrise.build.ItemBuildPresenter
import io.novajox.bitrise.build.ItemBuildView
import io.novajox.bitrise.project.ProjectVM
import io.novajox.data.pojo.BitriseBuild
import io.novajox.data.pojo.BitriseProject
import io.novajox.databinding.ProjectActivityBinding
import io.novajox.mvpvm.androidcomponent.BaseActivityView
import io.novajox.utils.EXTRA
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 * Created by novajox on 05/01/18.
 */
class DetailProjectView : BaseActivityView<DetailProjectPresenter, ProjectActivityBinding>() , DetailProjectContract.DetailProjectView{


    lateinit var project: BitriseProject
    lateinit var projectVM: ProjectVM
    override fun layout(): Int {
        return R.layout.project_activity
    }

    override fun binding(): Int {
        return BR.detailProjectView
    }

    var items: ObservableArrayList<ItemBuildView> = ObservableArrayList()
    var itemBinding = ItemBinding.of<ItemBuildView>(BR.itemBuildView, R.layout.build_rv_item)

    override fun createPresenter() {
        project = intent.getParcelableExtra(EXTRA.PROJECT)
        presenter = DetailProjectPresenter(this, project)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        projectVM = ProjectVM(project)
        setSupportActionBar(binding?.detailProjectViewToolbar)
        supportActionBar?.title = projectVM.title
        presenter?.loadData()
    }


    override fun update(list: List<BitriseBuild>) {
        for (app in list) {
            val buildVM = BuildVM(app)
            val item = ItemBuildView(buildVM)
            items.add(item)
            item.presenter = object : ItemBuildPresenter() {
                override fun onClickDownload() {
                    BottomArtifactsView.newInstance(project, app).show(supportFragmentManager, BottomArtifactsView::class.java.simpleName)
                }

                override fun onClickBuild() {
                    BottomArtifactsView.newInstance(project, app).show(supportFragmentManager, BottomArtifactsView::class.java.simpleName)
                    Toast.makeText(this@DetailProjectView, "Click on ${buildVM.date} build", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

}