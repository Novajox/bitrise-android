package io.novajox.data.api

import com.google.gson.GsonBuilder
import io.novajox.data.AppPrefs
import io.novajox.data.model.AppUser
import io.novajox.data.pojo.BitriseArtifact
import io.novajox.data.pojo.BitriseBuild
import io.novajox.data.pojo.BitriseProject
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


/**
 * Created by novajox on 18/12/17.
 */

enum class ApiManager {

    INSTANCE;

    private lateinit var endpoint: ApiEndpoint

    companion object {
        val FORMAT_DATE = "yyyy-MM-dd'T'HH:mm:ss'Z'"
        val API_DATE_FORMAT = SimpleDateFormat(FORMAT_DATE)
    }

    fun init() {

        val okHttp = OkHttpClient.Builder().addInterceptor({
            val request = it.request().newBuilder().addHeader(
                    "Authorization", "token ${AppPrefs.token()}"
            ).build()
            return@addInterceptor it.proceed(request)
        })
                .connectTimeout(20, TimeUnit.SECONDS)
                .readTimeout(20, TimeUnit.SECONDS)
                .writeTimeout(20, TimeUnit.SECONDS)
                .build()
        val gson = GsonBuilder()
                .setDateFormat(FORMAT_DATE)
                .create()
        val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(
                        RxJava2CallAdapterFactory.create())
                .addConverterFactory(
                        GsonConverterFactory.create(gson))
                .client(okHttp)
                .baseUrl("https://api.bitrise.io/v0.1/")
                .build()

        endpoint = retrofit.create(ApiEndpoint::class.java)
    }


    fun me(): Observable<AppUser> {
        return endpoint.me()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { return@map AppUser(it.data.username) }
    }


    fun myApps(): Observable<List<BitriseProject>> {
        return endpoint.myApps()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { return@map it.data }
    }

    fun appBuilds(slug: String, limit: Int): Observable<List<BitriseBuild>> {
        return endpoint.appBuilds(slug, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { return@map it.data }
    }


    fun lastAppBuild(slug: String): Observable<BitriseBuild> {
        return appBuilds(slug, 1).map {
            if (it.isNotEmpty())
                return@map it[0]
            else
                return@map null
        }
    }

    fun buildArtifacts(projectSlug: String, buildSlug: String, limit: Int): Observable<List<BitriseArtifact>> {
        return endpoint.buildArtifacts(projectSlug, buildSlug, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { return@map it.data }
    }


    fun artifactDetail(projectSlug: String, buildSlug: String, artifactSlug: String, limit: Int): Observable<BitriseArtifact> {
        return endpoint.artifactDetail(projectSlug, buildSlug, artifactSlug, limit)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .map { return@map it.data }
    }

}
