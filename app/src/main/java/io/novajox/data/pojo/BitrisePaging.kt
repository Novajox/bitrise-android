package io.novajox.data.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by novajox on 30/12/17.
 */
data class BitrisePaging(
        @SerializedName("total_item_count") var totalItemCount: Int,
        @SerializedName("page_item_limit") var pageItemLimit: Int
)