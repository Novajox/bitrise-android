package io.novajox.bitrise.artifact

import android.app.Activity
import android.databinding.ObservableArrayList
import io.novajox.BR
import io.novajox.R
import io.novajox.bitrise.artifact.item.ItemArtifactPresenter
import io.novajox.bitrise.artifact.item.ItemArtifactView
import io.novajox.bitrise.build.BuildVM
import io.novajox.bitrise.project.ProjectVM
import io.novajox.mvpvm.view.BaseView
import me.tatarka.bindingcollectionadapter2.ItemBinding

/**
 * Created by novajox on 07/01/18.
 */
class ArtifactsView(val activity: Activity, val buildVM: BuildVM, val projectVM: ProjectVM) : BaseView<ArtifactsPresenter>() {

    val items: ObservableArrayList<ItemArtifactView> = ObservableArrayList()
    var itemBinding = ItemBinding.of<ItemArtifactView>(BR.itemArtifactView, R.layout.artifact_rv_item)


    fun addItems(models: List<ItemArtifactView>) {
        for (item in models) {
            item.presenter = ItemArtifactPresenter(item)
            items.add(item)
            item.presenter.loadData()
        }
    }
}