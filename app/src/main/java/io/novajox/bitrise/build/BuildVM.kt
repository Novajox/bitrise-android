package io.novajox.bitrise.build

import android.databinding.Bindable
import io.novajox.R
import io.novajox.data.pojo.BitriseBuild
import io.novajox.mvpvm.vm.BaseVM
import java.util.*

/**
 * Created by novajox on 05/01/18.
 */
class BuildVM(model: BitriseBuild) : BaseVM<BitriseBuild>(model) {


    val statusText: String
        @Bindable
        get() = model.statusText

    val success: Boolean
        @Bindable
        get() = model.status == 1

    val date: Date
        @Bindable
        get() = model.triggeredAt

    val running: Boolean
        @Bindable
        get() = model.statusText == "in-progress"

    val slug: String
        @Bindable
        get() = model.slug

    val branch: String
        @Bindable
        get() = model.branch


    val buildNumber: String
        @Bindable
        get() = "# " + model.buildNumber

    val commitMessage: String?
        @Bindable
        get() = model.commitMessage

    val colorStatus: Int
        @Bindable
        get() =
            if (running) R.color.colorAccent
            else if (success) R.color.colorSuccess
            else R.color.colorError


}