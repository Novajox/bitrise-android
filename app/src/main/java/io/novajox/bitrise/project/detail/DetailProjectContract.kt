package io.novajox.bitrise.project.detail

import io.novajox.data.pojo.BitriseBuild
import io.novajox.mvpvm.view.IView

/**
 * Created by blackbird-linux on 17/01/18.
 */
interface DetailProjectContract {
    interface DetailProjectView : IView {
        fun update(list: List<BitriseBuild>) {}
        }
}