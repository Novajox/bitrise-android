package io.novajox.bitrise.download

import android.databinding.BaseObservable
import android.databinding.Bindable

/**
 * Created by novajox on 11/01/18.
 */
class DownloadVM(var soFarBytes: Int, var totalBytes: Int) : BaseObservable() {
    constructor() : this(0, 0)

    val moDownloaded: String
        @Bindable
        get() = "" + soFarBytes * 0.001f

    val moToDownload: String
        @Bindable
        get() = "" + totalBytes * 0.001f


    val status: String
        @Bindable
        get() = "$moDownloaded/$moToDownload"

    @Bindable
    fun getStatusPercent(): String {
        val percent = moDownloaded.toFloat() / moToDownload.toFloat() * 100
        val f = "%.2f".format(percent)
        return "$f %"
    }

    fun update(soFarBytes: Int, totalBytes: Int) {
        this.soFarBytes = soFarBytes
        this.totalBytes = totalBytes
        notifyChange()
    }

}