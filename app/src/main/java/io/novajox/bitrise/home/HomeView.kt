package io.novajox.bitrise.home

import android.content.Intent
import android.databinding.ObservableArrayList
import android.os.Bundle
import io.novajox.BR
import io.novajox.R
import io.novajox.bitrise.home.item.ItemProjectPresenter
import io.novajox.bitrise.home.item.ItemProjectView
import io.novajox.bitrise.project.ProjectVM
import io.novajox.bitrise.project.detail.DetailProjectView
import io.novajox.data.AppPrefs
import io.novajox.data.pojo.BitriseProject
import io.novajox.databinding.HomeActivityBinding
import io.novajox.mvpvm.androidcomponent.BaseActivityView
import io.novajox.utils.EXTRA
import me.tatarka.bindingcollectionadapter2.ItemBinding

class HomeView : BaseActivityView<HomePresenter, HomeActivityBinding>(), HomeContract.HomeView {

    override fun layout(): Int {
        return R.layout.home_activity
    }

    override fun binding(): Int {
        return BR.homeView
    }

    var items: ObservableArrayList<ItemProjectView> = ObservableArrayList()
    var itemBinding = ItemBinding.of<ItemProjectView>(BR.itemProjectView, R.layout.project_rv_item)

    override fun createPresenter() {
        presenter = HomePresenter(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initTitle()
        presenter?.loadData()
    }


    fun initTitle() {
        setSupportActionBar(binding!!.homeToolbar)
        supportActionBar?.title = "Welcome ${AppPrefs.user().username}"
    }

    override fun addItems(models: List<BitriseProject>) {
        for (app in models) {
            val item = ItemProjectView(ProjectVM(app))
            item.presenter = object : ItemProjectPresenter(item) {

                override fun onClickProject() {
                    startActivity(Intent(this@HomeView, DetailProjectView::class.java)
                            .putExtra(EXTRA.PROJECT, app))
                }
            }

            item.presenter.loadData()
            items.add(item)
        }
    }


}