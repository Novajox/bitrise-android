package io.novajox.bitrise.home

import io.novajox.data.pojo.BitriseProject
import io.novajox.mvpvm.view.BaseView
import io.novajox.mvpvm.view.IView

/**
 * Created by blackbird-linux on 17/01/18.
 */
interface HomeContract {
    interface HomeView : IView {
        fun addItems(models: List<BitriseProject>)
    }
}