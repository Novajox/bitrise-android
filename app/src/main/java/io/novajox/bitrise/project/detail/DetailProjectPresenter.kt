package io.novajox.bitrise.project.detail

import io.novajox.bitrise.project.IProjectPresenter
import io.novajox.data.api.ApiManager
import io.novajox.data.pojo.BitriseProject
import io.novajox.mvpvm.presenter.BasePresenter
import io.novajox.utils.AppLog

/**
 * Created by novajox on 05/01/18.
 */

class DetailProjectPresenter(projectView: DetailProjectContract.DetailProjectView,val model:BitriseProject) : BasePresenter<DetailProjectContract.DetailProjectView>(projectView), IProjectPresenter {


    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.appBuilds(model.slug, 50).subscribe({
            view.update(it)
            if (it.isEmpty())
                view.showEmptyState()
            view.hideLoadingState()
        }, {
            AppLog.d(it.localizedMessage)
            view.hideLoadingState()
            view.showErrorState()
        })
    }
}
