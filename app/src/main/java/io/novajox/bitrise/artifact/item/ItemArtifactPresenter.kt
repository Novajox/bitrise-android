package io.novajox.bitrise.artifact.item

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Environment
import android.support.v4.content.FileProvider
import android.webkit.MimeTypeMap
import com.liulishuo.filedownloader.BaseDownloadTask
import com.liulishuo.filedownloader.FileDownloadListener
import com.liulishuo.filedownloader.FileDownloader
import com.tbruyelle.rxpermissions2.RxPermissions
import io.novajox.data.api.ApiManager
import io.novajox.mvpvm.presenter.BasePresenter
import io.novajox.utils.AppLog
import java.io.File


/**
 * Created by novajox on 09/01/18.
 */
class ItemArtifactPresenter(itemArtifactView: ItemArtifactView) : BasePresenter<ItemArtifactView>(itemArtifactView) {

    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.artifactDetail(view.projectVM.slug, view.buildVM.slug, view.artifactVM.model.slug, 50).subscribe({
            view.artifactVM.update(it)
            view.downloadVM.update(0, it.fileSizeBytes)
            view.hideLoadingState()
        }, {
            view.hideLoadingState()
            view.showErrorState()
        })

    }


    fun onClickInstall(activity: Activity) {
        var intent = Intent(Intent.ACTION_INSTALL_PACKAGE)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path + "/build_" + view.buildVM.buildNumber + "_" + view.artifactVM.title)

        val mime = MimeTypeMap.getSingleton()
        val ext = file.name.substring(file.name.indexOf(".") + 1)
        val type = mime.getMimeTypeFromExtension(ext)
        val apkUr = FileProvider.getUriForFile(activity, activity.applicationContext.packageName + ".io.novajox.provider", file)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.setDataAndType(apkUr, type)
        activity.startActivity(intent)
    }

    fun onClickDownload(activity: Activity) {
        if (view.downloading.get()) {
            view.showMessageAlreadyDownloading()
            return
        }
        view.showDownloadingState()
        val rxPerm = RxPermissions(activity)
        if (rxPerm.isGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

            //TODO create notification with service to handle this
            FileDownloader.getImpl().create(view.artifactVM.url)
                    .setPath(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).path + "/build_" + view.buildVM.buildNumber + "_" + view.artifactVM.title)
                    .setListener(object : FileDownloadListener() {
                        override fun warn(task: BaseDownloadTask?) {
                            AppLog.d("warn")
                        }

                        override fun completed(task: BaseDownloadTask?) {
                            AppLog.d("completed")
                            view.hideDownloadingState()
                            view.showDownloadedState()
                        }

                        override fun pending(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                            AppLog.d("pending")
                        }

                        override fun error(task: BaseDownloadTask?, e: Throwable?) {
                            AppLog.d("error")
                            view.hideDownloadingState()
                            view.showErrorDownloadingState()
                        }

                        override fun progress(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                            view.updateDownload(soFarBytes, totalBytes)
                            AppLog.d("progress")
                        }

                        override fun paused(task: BaseDownloadTask?, soFarBytes: Int, totalBytes: Int) {
                            AppLog.d("paused")
                        }

                    })
                    .start()
        } else {
            rxPerm.request(Manifest.permission.WRITE_EXTERNAL_STORAGE).subscribe({
                onClickDownload(activity)
            }, {})
        }

    }
}