package io.novajox.bitrise.main

import android.content.Intent
import android.os.Bundle
import android.view.View
import io.novajox.BR
import io.novajox.R
import io.novajox.bitrise.home.HomeView
import io.novajox.databinding.MainActivityBinding
import io.novajox.mvpvm.androidcomponent.BaseActivityView
import io.novajox.utils.FragmentWebView
import kotlinx.android.synthetic.main.main_activity.*

class MainView : BaseActivityView<MainPresenter, MainActivityBinding>() {
    override fun binding(): Int {
        return BR.mainView
    }

    override fun layout(): Int {
        return R.layout.main_activity
    }


    override fun createPresenter() {
        presenter = MainPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter?.init()
    }

    fun handleClickBtNoToken(view: View) {
        FragmentWebView.newInstance("https://www.bitrise.io/me/profile").show(supportFragmentManager, "WebView")
    }

    fun handleClickGo(view: View) {
        presenter?.saveTokenValue()
        presenter?.loadData()
    }


    fun getEtTokenValue(): String {
        return et_token.text.toString()
    }

    fun goToHomeActivity() {
        startActivity(Intent(this, HomeView::class.java))
    }
}