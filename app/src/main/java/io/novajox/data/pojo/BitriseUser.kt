package io.novajox.data.pojo

import com.google.gson.annotations.SerializedName

/**
 * Created by novajox on 29/12/17.
 */
data class BitriseUser(
        @SerializedName("username") var username: String
)