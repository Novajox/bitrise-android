package io.novajox.bitrise.project

import android.databinding.Bindable
import io.novajox.data.AppPrefs
import io.novajox.data.pojo.BitriseProject
import io.novajox.mvpvm.vm.BaseVM

/**
 * Created by novajox on 04/01/18.
 */

class ProjectVM(model: BitriseProject) : BaseVM<BitriseProject>(model) {

    val title: String
        @Bindable
        get() = model.title

    val slug: String
        @Bindable
        get() = model.slug


    val badgeUrl: String
        @Bindable
        get() = "https://www.bitrise.io/app/${model.slug}/status.svg?token=${AppPrefs.token()}"


}
