package io.novajox.data.pojo;

import com.google.gson.annotations.SerializedName;

data class OriginalBuildParams(@SerializedName("branch") val branch: String //master
)
