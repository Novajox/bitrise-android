package io.novajox.bitrise.main

import io.novajox.data.AppPrefs
import io.novajox.data.api.ApiManager
import io.novajox.mvpvm.presenter.BasePresenter

/**
 * Created by novajox on 13/12/17.
 */

class MainPresenter(view: MainView) : BasePresenter<MainView>(view) {
    fun init() {
        if (AppPrefs.hasToken()) {
            loadData()
        } else {
            view.showEmptyState()
        }
    }


    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.me().subscribe({
            view.hideLoadingState()
            AppPrefs.putAppUser(it)
            view.goToHomeActivity()
        }, {

            view.showErrorState()
            view.hideLoadingState()
        })
    }


    fun saveTokenValue() {
        val token = view.getEtTokenValue()
        AppPrefs.putToken(token)
    }
}
