package io.novajox.bitrise.home.item

import io.novajox.bitrise.project.IProjectPresenter
import io.novajox.data.api.ApiManager
import io.novajox.mvpvm.presenter.BasePresenter
import io.novajox.utils.AppLog

/**
 * Created by novajox on 05/01/18.
 */

abstract class ItemProjectPresenter(itemProjectView: ItemProjectView) : BasePresenter<ItemProjectView>(itemProjectView), IProjectPresenter {

    override fun loadData() {
        view.showLoadingState()
        ApiManager.INSTANCE.lastAppBuild(view.vm.slug).subscribe({
            view.update(it)
            view.hideLoadingState()
        }, {
            AppLog.d(it.localizedMessage)
            view.hideLoadingState()
            view.showErrorState()
        })
    }

    abstract fun onClickProject()

}
